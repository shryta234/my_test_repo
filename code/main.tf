provider "aws" {
  region = "us-east-1"
}
 resource "aws_vpc" "test-vpc-master" {
     cidr_block = "10.0.0.0/16"
     enable_dns_support = true
     enable_dns_hostnames = true
     tags = {
       "Name" = "test-vpc-master"
     }
   
 }
 resource "aws_subnet" "test-subnet-pub" {
  vpc_id     = aws_vpc.test-vpc-master.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "test-subnet-pub"
  }
}
resource "aws_internet_gateway" "gw" {
  vpc_id     = aws_vpc.test-vpc-master.id

  tags = {
    Name = "main"
  }
}
resource "aws_route_table" "pub-rt" {
  vpc_id     = aws_vpc.test-vpc-master.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "pub-rt"
  }
}
resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.test-subnet-pub.id
  route_table_id = aws_route_table.pub-rt.id
}
resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"
  vpc_id      = aws_vpc.test-vpc-master.id

  ingress {
    description      = "SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

 ingress {
    description      = "HTTP"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

 ingress {
    description      = "HTTPS"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_ssh"
  }
}
resource "aws_key_pair" "ssh-key" {
  key_name   = "ssh-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC7sP+hGNHueXIhgSbpQgoph3C/lRXho6ZqUzvDcsA2laJRr9wCGJV0xhQ5BTw6agt668+aTf+FD3ZCZYkW905sm4l8DwgwD+s7wfmzpPbmapPePxNIh6FSPkwXl2aGSWpL9e02+UT2P5ARu4Cc2JrfJQkYFUHCaaB2OgM6K/DI5ZIwORO98Kb5mngKP8ALScrFI1nC4BLEtsUXKYvsELroJZ4ZTk/+bDRzuaOCnc7ItLPgK8D04S0WofvdvpNUlj/gsR06nksHjzI08GfIYfjxMzqK7VFgIdczpifByo2nKyLksL+puQwOGmjWlMXrKVfIwyI1bRvsmKOVNWPXOgjN ec2-user@ip-172-31-1-167.ec2.internal"
}
resource "aws_instance" "web" {
  ami               = "ami-0c02fb55956c7d316"
  subnet_id         = aws_subnet.test-subnet-pub.id
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]
  instance_type     = "t2.micro"
  key_name          = "ssh-key"

  user_data = <<-EOF
  #!/bin/bash
  echo "*** Installing apache2"
  sudo yum update -y
  sudo yum install -y httpd
  sudo systemctl start httpd
  sudo systemctl enable httpd
  sudo systemctl is-enabled httpd
  echo "*** Completed Installing apache2"
  EOF

  tags = {
    Name = "HelloWorld"
  }
}

resource "aws_eip" "my-eip" {
  vpc = true
}
resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.web.id
  allocation_id = aws_eip.my-eip.id
}

